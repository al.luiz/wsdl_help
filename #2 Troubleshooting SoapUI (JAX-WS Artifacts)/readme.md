# Como validar um WSDL no SoapUI (JAX-WS)

1. Instale o SoapUI em sua máquina:

    https://www.soapui.org/downloads/soapui/

2. Ao abrir o programa navegue até o menu 'Tools > JAX-WS Artifacts'

3. Clique em 'Tools'

4. Configure a pasta do 'JAX-WS WSImport' (Vide #1 Troubleshooting JAX-WS Parser)

5.
    
    Pronto! Este contrato WSDL está devidamente validado para ser utilizado em plataformas baseada em tecnologia JAVA.