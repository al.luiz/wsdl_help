# Links Uteis

## 1. Introduction to JAX-WS
Java API for XML Web Services (JAX-WS) is a standardized API for creating and consuming SOAP (Simple Object Access Protocol) web services.

In this article, we'll create a SOAP web service and connect to it using JAX-WS.

https://www.baeldung.com/jax-ws