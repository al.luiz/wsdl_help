# Como validar um WSDL no JAX-WS

1. Navegue até o diretório de instalação do Java (Versão acima de 6.x) em seu SO (Windows nesse caso):

    ```
    $ cd %JAVA_HOME%
    ```

2. Então navegue até o diretório '/bin':

    ```
    $ cd bin
    ```

3. Execute 'wsimport.exe':

    ```
    $ wsimport.exe <arquivo_wsdl>
    ```
    ### Exemplo

    O resultado do comando abaixo será um erro de validação do WSDL

    ```
    $ wsimport https://webservices.twwwireless.com.br/reluzcap/wsreluzcap.asmx?wsdl
    ```
    O resultado é uma exceção:

    ```
    ....

    [WARNING] src-resolve.4.2: Erro ao resolver o componente 's:schema'. Foi detectado que 's:schema' está no namespace 'http://www.w3.org/2001/XMLSchema', mas não é possível fazer referência aos componentes em namespace de destino usando o documento do esquema 'https://webservices.twwwireless.com.br/reluzcap/wsreluzcap.asmx?wsdl#types?schema1'. Se este for o namespace incorreto, talvez o prefixo de 's:schema' precise ser alterado. Se este for o namespace correto, então a tag de "importação" apropriada deverá ser adicionada a 'https://webservices.twwwireless.com.br/reluzcap/wsreluzcap.asmx?wsdl#types?schema1'.
    linha 228 de https://webservices.twwwireless.com.br/reluzcap/wsreluzcap.asmx?wsdl#types?schema1
    ...

    Exception in thread "main" com.sun.tools.internal.ws.wscompile.AbortException
            at com.sun.tools.internal.ws.processor.modeler.wsdl.JAXBModelBuilder.bind(JAXBModelBuilder.java:129)
            at com.sun.tools.internal.ws.processor.modeler.wsdl.WSDLModeler.buildJAXBModel(WSDLModeler.java:2283)
            at com.sun.tools.internal.ws.processor.modeler.wsdl.WSDLModeler.internalBuildModel(WSDLModeler.java:183)
            at com.sun.tools.internal.ws.processor.modeler.wsdl.WSDLModeler.buildModel(WSDLModeler.java:126)
            at com.sun.tools.internal.ws.wscompile.WsimportTool.buildWsdlModel(WsimportTool.java:429)
            at com.sun.tools.internal.ws.wscompile.WsimportTool.run(WsimportTool.java:190)
            at com.sun.tools.internal.ws.wscompile.WsimportTool.run(WsimportTool.java:168)
            at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
            at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
            at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
            at java.lang.reflect.Method.invoke(Method.java:498)
            at com.sun.tools.internal.ws.Invoker.invoke(Invoker.java:159)
            at com.sun.tools.internal.ws.WsImport.main(WsImport.java:42)

    ```
    O wsimport está nos dizendo que o namespace não foi importado corretamente, portanto, devemos ajustar a importação antes de prosseguir. Para isso, vamos baixar o WSDL e editá-lo localmente em nossa máquina.

4. Editando o WSDL

    Após baixar o arquivo vamos entender o que houve. Durante o parser o JAX-WS identificou que o contrato está tentando <b>referenciar</b> o namespace '<b>s:schema</b>' na linha 228. Como podemos observar a seguir:

    ```
    <s:complexType>
        <s:sequence>
            <s:element ref="s:schema" />
            <s:any />
        </s:sequence>
    </s:complexType>
    ```

    Esse namespace se refere ao <b>namespace='http://www.w3.org/2001/XMLSchema'</b> conforme o próprio JAX-WS identificou:

    >..Foi detectado que 's:schema' está no namespace 'http://www.w3.org/2001/XMLSchema', mas não é possível fazer referência aos componentes em namespace de destino usando o documento do esquema.."

    Em outras palavras, isso significa que o JAX-WS não sabe de onde vem a definição do seu componente, pois isso não está explícito no contrato. Portanto, devemos importar esse namespace para utilizá-lo.

    Para isso vamos acrescentar o seguinte trecho de código logo abaixo do schema:

    ```
    ...
    
    <s:schema elementFormDefault="qualified" targetNamespace="https://www.twwwireless.com.br/reluzcap/wsreluzcap">
        <s:import namespace="http://www.w3.org/2001/XMLSchema" schemaLocation="http://www.w3.org/2001/XMLSchema.xsd"/>
    ...
    ```
    Onde estamos dizendo o seguinte:
    
    Meu namespace="http://www.w3.org/2001/XMLSchema" está localizado em schemaLocation="http://www.w3.org/2001/XMLSchema.xsd"

    E assim ajustamos nossa importação.

5. Como iremos acessar um namespace externo, devemos acrescentar os comandos ao nosso parser:

    ```
    javax.xml.accessExternalDTD=all
    javax.xml.accessExternalSchema=all
    ```


    Resultando em:

    ```
    $ wsimport -J-Djavax.xml.accessExternalDTD=all -J-Djavax.xml.accessExternalSchema=all wsreluzcap.xml
    ```

    E se tudo ocorreu conforme o esperado a seguinte mensagem deve aparecer:

    ```
    
    Gerando o código...
    
    ...
    
    Compilando o código...
    ```
    E os arquivos compilados estarão no diretório que você estiver quando rodou o parser:

    ```
    $ dir /b /s *.class
    ...

    C:\Users\allui\web_services\https\www_twwwireless_com_br\reluzcap\wsreluzcap\BuscaSMS.class
    C:\Users\allui\web_services\https\www_twwwireless_com_br\reluzcap\wsreluzcap\BuscaSMSAgenda.class
    C:\Users\allui\web_services\https\www_twwwireless_com_br\reluzcap\wsreluzcap\BuscaSMSAgendaDataSet$DS.class
    C:\Users\allui\web_services\https\www_twwwireless_com_br\reluzcap\wsreluzcap\BuscaSMSAgendaDataSet.class
    C:\Users\allui\web_services\https\www_twwwireless_com_br\reluzcap\wsreluzcap\BuscaSMSAgendaDataSetResponse$BuscaSMSAgendaDataSetResult.class

    ...
    ```
    Pronto! Este contrato WSDL está devidamente validado para ser utilizado em plataformas baseada em tecnologia JAVA.